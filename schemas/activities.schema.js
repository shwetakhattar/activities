const Joi = require('joi');

const schemas = {
   title: Joi.string().alphanum().min(3).max(30).required()
};

module.exports = schemas;