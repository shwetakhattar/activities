﻿require('rootpath')();
let mysql = require('mysql');
let envVars = require('dotenv').config();
const hostname = process.env.HOST;
const port = process.env.PORT;
let express = require('express');
let bodyParser = require('body-parser');
let app = express();
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
let sql = require('config/db');
let activitiesRoutes = require('./routes/activity.js');
app.use('/activities', activitiesRoutes);
let server = app.listen(port, () => {
  console.log('Server listening on port ' + port);
});

