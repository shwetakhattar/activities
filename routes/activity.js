let express = require('express');
let mysql = require('mysql');
let ctrl = require('controllers/activities.controller.js');
let router = express.Router();
router
  .get('/:id', ctrl.getById)
  .put('/:id', ctrl.update)
  .delete('/:id', ctrl.delete);
router
  .post('/',ctrl.create)
  .get('',ctrl.listing);
module.exports = router;
