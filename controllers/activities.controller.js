const Joi = require('joi'); 
const schema = Joi.object().keys({ 
  name: Joi.string().alphanum().min(3).max(30).required(),
  birthyear: Joi.number().integer().min(1970).max(2013), 
}); 
var Activity = require('models/activity.model.js');
exports.getById = function(req, res) {
  Activity.getById(req.params.id, function(err, activity) {
    if (err)
      res.send(err);
    res.json(activity);
  });
};

exports.update = function(req, res) {
  var ActivityBody = new Activity(req.body);
  Activity.update(req.params.id, ActivityBody, function(err, activity) {
    if (err)
      res.send(err);
    res.json(activity);
  });
};

exports.delete = function(req, res) {
  Activity.remove(req.params.id, function(err, activity) {
    if (err)
      res.send(err);
    res.json(activity);
  });
};

exports.listing = function(req, res) {
  Activity.getAll(function(err, activity) {
    if (err)
      res.send(err);
    res.json(activity);
  });
};

exports.create = function(req, res) {
  var new_activity = new Activity(req.body);
  Activity.create(new_activity, function(err, activity) {
    if (err)
      res.send(err);
    res.json(activity);
  });
};



