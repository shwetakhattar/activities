let mysql = require('mysql');
let con = mysql.createConnection({
  host: process.env.HOST,
  user: process.env.USER,
  password: process.env.PASSWORD,
  database: process.env.DATABASE,
  multipleStatements : true  
});
con.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
});
module.exports = con;