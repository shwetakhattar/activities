let messages = {
  'FIRST_NAME': 'FirstName is required.',
  'LAST_NAME': 'LastName is required.',
  'EMAIL': 'Email is required.',
  'EMAIL_EXIST': 'Email already exists.',
  'USERNAME_EXIST': 'Username already exists',
  'INVALIDATE_DETAILS': 'Username or Password is incorrect.',
  'ERROR': 'Error.',
  'SUCCESS': 'Success.',
  'SAVE_DATA': 'Data successfully saved.',
  'SAVE_ERROR': 'Problem saving data.',
  'UPDATE_DATA': 'Data updated successfully.',
  'UPDATE_ERROR': 'Error updating data.',
  'STATUS_DATA': 'Status changed successfully.',
  'STATUS_ERROR': 'Problem changing status.',
  'LIST_DATA': 'Data listed successfully.',
  'LIST_ERROR': 'Error retrieving data.',
  'CORRECT_PASSWORD': 'Password is correct',
  'INCORRECT_PASSWORD': 'Incorrect Password',
  'PLAN_EXISTS': 'Plan name already exists.',
  // 'url': {
  //   'imageUrl': 'http://54.71.18.74:4559',
  //   'quoteUrl': 'http://54.71.18.74:4557/#/quote/',
  //   'userUrl': 'http://54.71.18.74:4557/#/user/',
  //   'logoUrl': 'http://54.71.18.74:4559/images/logo.png',
  // },

  'folders': {
    task_docs: 'public/task_docs',
  },
};

module.exports = messages;
