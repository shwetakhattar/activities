﻿const mysql = require('mysql');
const con = require('config/db');

var Activity = function(activity){
    this.activity = activity.activity;
    this.status = activity.status;
    this.created_at = new Date();
};

Activity.getById = function (id, result) {
        con.query("Select * from activities where Id = ? ",id, function (err, res) {         
            if(err) {
                result(err, null);
            }
            else{
                result(null, res);
            }
        });   
};

Activity.getAll = function (result) {
        con.query("Select * from activities", function (err, res) {         
            if(err) {
                result(err, null);
            }
            else{
                result(null, res);
            }
        });   
};

Activity.remove = function (id, result) {
        con.query("DELETE FROM activities WHERE id = ?", [id], function (err, res) {        
            if(err) {
                result(err, null);
            }
            else{
                result(null, res);
            }
        });   
};

Activity.update = function (id, activity, result) {
        con.query("UPDATE activities SET title = ? WHERE id = ?", [activity.title, id], function (err, res) {        
            if(err) {
                result(err, null);
            }
            else{
                result(null, res);
            }
        });   
};

Activity.create = function (newActivity, result) {
       con.query("INSERT INTO activities set ?", newActivity, function (err, res) {
            if(err) {
                console.log("error: ", err);
                result(err, null);
            }
            else{
                console.log(res.insertId);
                result(null, res.insertId);
            }
        });   
};

module.exports= Activity;